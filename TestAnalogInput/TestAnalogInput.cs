﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MccDaq;
using LibAnalogTools;
using System.Threading;

namespace TestAnalogInput
{
    class TestAnalogInput
    {
        private MccBoard acqBoard;
        private AnalogInput analogInput;
        public TestAnalogInput() 
        {
            this.acqBoard = new MccBoard(0);
            this.analogInput = new AnalogInput(acqBoard, 7);
        }
       
        public void Run()
        {
            while (true) 
            {
               var data = analogInput.MesureBrute;
               var dataV = analogInput.MesureVolt;

                Console.WriteLine("value  : " + data.ToString());
                Console.WriteLine("value lumiere en V : " + dataV.ToString());
                Console.WriteLine("");

                Thread.Sleep(1000);
            }
        }

        static void Main(string[] args)
        {
           var testAnalogInput = new TestAnalogInput();
            testAnalogInput.Run();
        }
    }  
}
