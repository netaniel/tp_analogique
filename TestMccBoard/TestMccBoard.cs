﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MccDaq;
using System.Threading;

namespace TestMccBoard
{
    class TestMccBoard
    {
        static void Main(string[] args)
        {

            UInt16 dataValueLum;
            float dataValueLumV;
            UInt16 dataValueTemp;
            float dataValueTempV;

            Range range = Range.Bip10Volts;
            int channelLum = 7;
            int channelTemp = 6;
            VInOptions option = 0;
            MccBoard mccBoard7 = new MccBoard(0);

            while (true)
            {
                //lumiere 
                 mccBoard7.AIn(channelLum, range, out dataValueLum);
                 mccBoard7.VIn(channelLum, range, out dataValueLumV, option);

                //temperature
                mccBoard7.AIn(channelTemp, range, out dataValueTemp);
                mccBoard7.VIn(channelTemp, range, out dataValueTempV, option);

                //affichage des valeurs
                Console.WriteLine("value lumiere : " + dataValueLum.ToString());
                Console.WriteLine("value lumiere en V : " + dataValueLumV.ToString());
                Console.WriteLine("");
                Console.WriteLine("value temperature : " + dataValueTemp.ToString());
                Console.WriteLine("value temperature en V : " + dataValueTempV.ToString());
                Console.WriteLine("");
                

                Thread.Sleep(1000);
            }
            Console.Read();
        }
    }
}
