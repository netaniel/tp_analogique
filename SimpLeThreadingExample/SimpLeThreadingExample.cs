﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpLeThreadingExample
{
    class SimpLeThreadingExample
    {

        static void Main(string[] args)
        {
            int iter = 6;
            ManualResetEvent SignalExit = new ManualResetEvent(false);
            AutoResetEvent SignalVoyelle = new AutoResetEvent(false);

            // Création des objets encapsulant les threads.
            MyThreadLetter ThLetter = new MyThreadLetter(SignalExit, SignalVoyelle);
            MyThreadNumber ThCptr = new MyThreadNumber(SignalExit, SignalVoyelle);
            Console.WriteLine("> Le thread est créé.");

            ThLetter.Start();
            ThCptr.Start(19);

            Console.WriteLine("");
            Console.WriteLine("> En route pour {0}sec. ...", iter);
            for (int i = 0; i < iter; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("> main: " + i + "sec.");
            }

            Console.WriteLine("> Demmande d’arrêt du thread.");
            
            SignalExit.Set();

            // On attend que le thread en cours soit terminés avant de continuer.
            ThLetter.LaTache.Join();
            ThCptr.LaTache.Join();

            Console.WriteLine("> Les threads sont terminés.");
            Console.WriteLine("\n\n\t\tTouche \"Entrée\" pour terminer...");
            Console.ReadLine();
        }
    }
}
